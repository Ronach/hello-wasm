// wasm_bindgen module coming from wasm-pack crate (equivalent of library in cargo)
// provide a bridge between the types of JavaScript and Rust. 
// It allows JavaScript to call a Rust API with a string, or a Rust function to catch a JavaScript exception.
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern {
    pub fn alert(s: &str);
}

#[wasm_bindgen]
pub fn greet(name: &str) {
    alert(&format!("Hello, {}!", name));
}