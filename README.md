wasm : https://webassembly.org/
1) google chrome
2) firefox
3) safari
4) microsoft edge

projet : https://developer.mozilla.org/en-US/docs/WebAssembly/Rust_to_wasm
Compiler du Rust en WebAssembly pour servir en front et le proposer en package node

1) cargo new --lib hello-wasm
2) git config --global -l       : pour remplir 'authors' dans Cargo.toml
3) wasm-pack build --target web : crée pkg/  
-- create an index.html file --
4) python -m http.server        : crée un serveur web python local sur le port 8000 http://localhost:8080
5) wasm-pack build --target bundler : to use the WebAssembly module with npm
6) choco install nodejs  
PS C:\Users\loann> node --version  
v21.1.0  
PS C:\Users\loann> npm --version  
10.2.0  
7) Set-Location pkg/ && npm link
8) Set-Location .. && New-Item -Directory site
9) Set-Location site && npm link hello-wasm  
-- create a new package.json file --    
From site/ :
10) npm install
11) npm run serve                : http://localhost:8080
